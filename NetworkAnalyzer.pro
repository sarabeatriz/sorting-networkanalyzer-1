#-------------------------------------------------
#
# Project created by QtCreator 2014-05-27T16:16:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NetworkAnalyzer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    packet.cpp \
    File.cpp \
    Filter.cpp \
    Sort.cpp

HEADERS  += mainwindow.h \
    packet.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    logo.png

RESOURCES += \
    images.qrc \
    styles.qrc
