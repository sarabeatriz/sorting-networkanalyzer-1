#include <vector>
#include "packet.h"

/// \file

/// \fn void FilterBySrcAddr(vector<Packet> &netdata, string address)
/// \~English
/// Function that filters the packets in the netflow by the source address
/// \param netdata packets vector
/// \param address key address to filter
/// \~Spanish
/// Funcion que filtra los paquetes en el flujo de red por la direccion fuente
/// \param netdata vector de paquetes
/// \param address direccion llave para el fitro
void FilterBySrcAddr(vector<Packet> &netdata, string address){
    
     // YOUR CODE HERE


}

/// \fn void FilterByDstAddr(vector<Packet> &netdata, string address)
/// \~English
/// Function that filters the packets in the netflow by the destination address
/// \param netdata packets vector
/// \param address key address to filter
/// \~Spanish
/// Funcion que filtra los paquetes en el flujo de red por la direccion destino
/// \param netdata vector de paquetes
/// \param address direccion llave para el fitro
void FilterByDstAddr(vector<Packet> &netdata, string address){
    
     // YOUR CODE HERE

}

/// \fn void FilterBySrcPort(vector<Packet> &netdata, int port)
/// \~English
/// Function that filters the packets in the netflow by the source port
/// \param netdata packets vector
/// \param port key port to filter
/// \~Spanish
/// Funcion que filtra los paquetes en el flujo de red por el puerto fuente
/// \param netdata vector de paquetes
/// \param port puerto llave para el fitro
void FilterBySrcPort(vector<Packet> &netdata, int port){
    
     // YOUR CODE HERE


}

/// \fn void FilterByDstPort(vector<Packet> &netdata, int port)
/// \~English
/// Function that filters the packets in the netflow by the destination port
/// \param netdata packets vector
/// \param port key port to filter
/// \~Spanish
/// Funcion que filtra los paquetes en el flujo de red por el puerto destino
/// \param netdata vector de paquetes
/// \param port puerto llave para el fitro
void FilterByDstPort(vector<Packet> &netdata, int port){

     // YOUR CODE HERE

}
