#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <iostream>
#include <fstream>
#include <QComboBox>
#include <QMenuBar>
#include <QLabel>
#include <QTableWidget>
#include <QCheckBox>
#include <QLineEdit>
#include <QFileDialog>
#include <QMessageBox>
#include <QApplication>
#include "packet.h"

using namespace std;

namespace Ui {
class MainWindow;
}

/// \fn bool ReadFile(string fname, vector<Packet> & netdata)
/// \~English
/// \brief Function that reads a file and return a boolean
/// that determines if the file was succesfully opened and read or not.
/// \param fname  String that contains the filename of the file.
/// \param netdata  Vector of packets.
/// \return true if successfuly read a file, false otherwise
/// \~Spanish
/// \brief Funcion que lee un archivo y devuelve un booleano que determina
/// si el archivo abrio y se leyo con exito o no.
/// \param fname  Cadena de caracteres que contiene el nombre del archivo.
/// \param netdata  Vector de paquetes.
/// \return cierto si leyo el archivo con exito, false de lo contrario
bool ReadFile(string, vector<Packet> &) ;

/// \fn bool SaveFile(string fname, vector<Packet> & netdata)
/// \~English
/// \brief Function that saves a file to the computer.
/// It returns true when the file was succesfully saved and false
/// if an error occurred.
/// \param fname  String that contains the filename of the file.
/// \param netdata  Vector of packets.
/// \return true if successfuly read a file, false otherwise
/// \~Spanish
/// \brief Funcion que guarda un archivo en la computadora.
/// Devuelve cierto cuando el archivo se guardo con exito y falso si
/// ocurre error
/// \param fname  Cadena de caracteres que contiene el nombre del archivo.
/// \param netdata  Vector de paquetes.
/// \return cierto si guardo el archivo con exito, false de lo contrario
bool SaveFile(string, vector<Packet>) ;

/// \fn void FilterBySrcAddr(vector<Packet> &netdata, string address)
/// \~English
/// Function that filters the packets in the netflow by the source address
/// \param netdata packets vector
/// \param address key address to filter
/// \~Spanish
/// Funcion que filtra los paquetes en el flujo de red por la direccion fuente
/// \param netdata vector de paquetes
/// \param address direccion llave para el fitro
void FilterBySrcAddr(vector<Packet> &, string );

/// \fn void FilterByDstAddr(vector<Packet> &netdata, string address)
/// \~English
/// Function that filters the packets in the netflow by the destination address
/// \param netdata packets vector
/// \param address key address to filter
/// \~Spanish
/// Funcion que filtra los paquetes en el flujo de red por la direccion destino
/// \param netdata vector de paquetes
/// \param address direccion llave para el fitro
void FilterByDstAddr(vector<Packet> &, string );

/// \fn void FilterBySrcPort(vector<Packet> &netdata, string port)
/// \~English
/// Function that filters the packets in the netflow by the source port
/// \param netdata packets vector
/// \param port key port to filter
/// \~Spanish
/// Funcion que filtra los paquetes en el flujo de red por el puerto fuente
/// \param netdata vector de paquetes
/// \param port puerto llave para el fitro
void FilterBySrcPort(vector<Packet> &, int) ;

/// \fn void FilterByDstPort(vector<Packet> &netdata, string port)
/// \~English
/// Function that filters the packets in the netflow by the destination port
/// \param netdata packets vector
/// \param port key port to filter
/// \~Spanish
/// Funcion que filtra los paquetes en el flujo de red por el puerto destino
/// \param netdata vector de paquetes
/// \param port puerto llave para el fitro
void FilterByDstPort(vector<Packet> &, int) ;

/// \fn void SortBySrcAddr(vector<Packet> &netdata)
/// \~English
/// \brief Function that sorts by source address
/// the packets in the netflow file using the Bubble sort algorithm.
/// \param netdata  Packet vector that will be sorted.
/// \~Spanish
/// \brief Function que ordena por direccion fuente
/// los paquetes en el archivo de netflows usando el algoritmo de Bubble sort (Burbuja).
/// \param netdata Vector de paquetes a ser ordenado.
void SortBySrcAddr(vector<Packet> &) ;

/// \fn void SortByDstAddr(vector<Packet> &netdata)
/// \~English
/// \brief Function that sorts by destination address
/// the packets in the netflow file using the Selection sort algorithm.
/// \param netdata  Packet vector that will be sorted.
/// \~Spanish
/// \brief Function que ordena por direccion destino
/// los paquetes en el archivo de netflows usando el algoritmo de Selection sort (Seleccion).
/// \param netdata Vector de paquetes a ser ordenado.
void SortByDstAddr(vector<Packet> &) ;

/// \fn void SortBySrcPort(vector<Packet> &netdata)
/// \~English
/// \brief Function that sorts by source port
/// the packets in the netflow file using the Bubble sort algorithm.
/// \param netdata  Packet vector that will be sorted.
/// \~Spanish
/// \brief Function que ordena por puerto fuente
/// los paquetes en el archivo de netflows usando el algoritmo de Selection sort (Burbuja).
/// \param netdata Vector de paquetes a ser ordenado.
void SortBySrcPort(vector<Packet> &) ;

/// \fn void SortByDstPort(vector<Packet> &netdata)
/// \~English
/// \brief Function that sorts by destination port
/// the packets in the netflow file using the Bubble sort algorithm.
/// \param netdata  Packet vector that will be sorted.
/// \~Spanish
/// \brief Function que ordena por puerto destino
/// los paquetes en el archivo de netflows usando el algoritmo de Bubble sort (Burbuja).
/// \param netdata Vector de paquetes a ser ordenado.
void SortByDstPort(vector<Packet> &) ;

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    int numPackets, numOctects;
    int filterMethod;
    vector< Packet > netdata;   // Used to store the data from the network file
    QTableWidgetItem *item;      //This pointer is used to create table entries and add them to the table

public:

    /// \fn MainWindow::MainWindow()
    /// \~English
    /// \brief Constructor
    /// \~Spanish
    /// \brief Constructor
    explicit MainWindow(QWidget *parent = 0);

    /// \fn MainWindow::~MainWindow()
    /// \~English
    /// \brief Destructor
    /// \~Spanish
    /// \brief Destructor
    ~MainWindow();

private slots:

    /// \fn void MainWindow::on_Filter_Box_currentIndexChanged(int index)
    /// \~English
    /// \brief Changes the ID of the filtering function chosen by the user
    /// \~Spanish
    /// \brief Cambia el ID de la funcion de filtro escogida por el usaurio
    void on_Filter_Box_currentIndexChanged(int index); //

    /// \fn void MainWindow::on_actionLoad_Network_Data_triggered()
    /// \~English
    /// \brief Loads a network flow file into the GUI.
    /// \~Spanish
    /// \brief Carga un archivo de fluidos de red a la inteface de usuario.
    void on_actionLoad_Network_Data_triggered(); //

    /// \fn void MainWindow::on_actionAbout_triggered()
    /// \~English
    /// \brief Displays a short about of the app
    /// \~Spanish
    /// \brief Despliega un sobre corto de la aplicacion
    void on_actionAbout_triggered(); //

    /// \fn void MainWindow::on_actionExit_triggered()
    /// \~English
    /// \brief Exits the app
    /// \~Spanish
    /// \brief Sale de la aplicacion
    void on_actionExit_triggered();

    /// \fn void MainWindow::FillTable()
    /// \~English
    /// \brief Refresh the GUI packet list.
    /// \~Spanish
    /// \brief Rellena la lista de paquetes de la interface de usuarios.
    void FillTable();

    /// \fn void MainWindow::on_runFilter_clicked()
    /// \~English
    /// \brief Invokes a filtering funtion that filter by the field chosen by the user.
    /// \~Spanish
    /// \brief Invoca una funcion de filtrar por el campo escogido por el usuario.
    void on_runFilter_clicked();

    /// \fn void MainWindow::on_sortButton_clicked()
    /// \~English
    /// \brief Invokes a sorting funtion that sorts by the field chosen by the user.
    /// \~Spanish
    /// \brief Invoca una funcion de ordenar por el campo escogido por el usuario.
    void on_sortButton_clicked();

    /// \fn void MainWindow::on_openFile_clicked()
    /// \~English
    /// \brief Open a file network flow packets.
    /// \~Spanish
    /// \brief Abre un archivo con paquetes de fluidos de red.
    void on_openFile_clicked();

    /// \fn void MainWindow::on_saveFile_clicked()
    /// \~English
    /// \brief Saves the network packets as appears in the window.
    /// \~Spanish
    /// \brief Guarda los paquetes de network como aparecen en la pantalla.
    void on_saveFile_clicked();

    /// \fn void MainWindow::on_clearButton_clicked()
    /// \~English
    /// \brief Removes any applied filter to the list of packets.
    /// \~Spanish
    /// \brief Remueve cualquier filtro aplicado a la lista de paquetes.
    void on_clearButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
