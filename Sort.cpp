#include <string>
#include <vector>
#include "packet.h"
#include <iostream>

/// \file

/// \fn void SortBySrcAddr(vector<Packet> &netdata)
/// \~English
/// \brief Function that sorts by source address
/// the packets in the netflow file using the Bubble sort algorithm.
/// \param netdata  Packet vector that will be sorted.
/// \~Spanish
/// \brief Function que ordena por direccion fuente
/// los paquetes en el archivo de netflows usando el algoritmo de Bubble sort (Burbuja).
/// \param netdata Vector de paquetes a ser ordenado.
void SortBySrcAddr(vector<Packet> &netdata){

    // Space to implement the Bubble Sort algorithm
   
    
    // YOUR CODE HERE


}


/// \fn void SortByDstAddr(vector<Packet> &netdata)
/// \~English
/// \brief Function that sorts by destination address
/// the packets in the netflow file using the Selection sort algorithm.
/// \param netdata  Packet vector that will be sorted.
/// \~Spanish
/// \brief Function que ordena por direccion destino
/// los paquetes en el archivo de netflows usando el algoritmo de Selection sort (Seleccion).
/// \param netdata Vector de paquetes a ser ordenado.
void SortByDstAddr(vector<Packet> &netdata){

    // Space to implement the Selection Sort algorithm
    
    
    // YOUR CODE HERE

}

/// \fn void SortBySrcPort(vector<Packet> &netdata)
/// \~English
/// \brief Function that sorts by source port
/// the packets in the netflow file using the Bubble sort algorithm.
/// \param netdata  Packet vector that will be sorted.
/// \~Spanish
/// \brief Function que ordena por puerto fuente
/// los paquetes en el archivo de netflows usando el algoritmo de Selection sort (Burbuja).
/// \param netdata Vector de paquetes a ser ordenado.
void SortBySrcPort(vector<Packet> &netdata){

    // Space to implement the Selection Sort algorithm
    
    
    // YOUR CODE HERE
    
}

/// \fn void SortByDstPort(vector<Packet> &netdata)
/// \~English
/// \brief Function that sorts by destination port
/// the packets in the netflow file using the Bubble sort algorithm.
/// \param netdata  Packet vector that will be sorted.
/// \~Spanish
/// \brief Function que ordena por puerto destino
/// los paquetes en el archivo de netflows usando el algoritmo de Bubble sort (Burbuja).
/// \param netdata Vector de paquetes a ser ordenado.
void SortByDstPort(vector<Packet> &netdata){

    // Space to implement the bubble sort algorithm
    
    
     // YOUR CODE HERE


}
